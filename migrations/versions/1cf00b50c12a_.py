"""empty message

Revision ID: 1cf00b50c12a
Revises: 2e7ae85404e2
Create Date: 2023-04-20 15:39:28.935441

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '1cf00b50c12a'
down_revision = '2e7ae85404e2'
branch_labels = None
depends_on = None

def upgrade():
    op.create_table('nodes',
        sa.Column('id', sa.Text(), nullable=False),
        sa.Column('info', sa.JSON(), nullable=False),
        sa.PrimaryKeyConstraint('id', name=op.f('pk_nodes'))
    )

def downgrade():
    op.drop_table('nodes')
