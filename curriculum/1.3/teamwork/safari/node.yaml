name: Business safari
goals:
    employee: 1
    roles: 1
    projectmanagement: 1
description: Experience life at a software company!
public: users
grading: false
avg_attempts: 1
start_when_ready: true

assignment:
    Business safari: |
        <img src="safari.png" class="side">

        Three times a year, we're organizing a *Business safari* day. During such a day, local software development companies are opening their doors to our students! 

        In the week leading up to the business safari, the list of participating companies will be made available. It will also include some basic information about each company, how many spots the company has available, and what your day at the company would look like.

        The basic idea is that the company should at least provide you with a desk and WiFi. You can bring your laptop and work on your school assignment, while getting a feel for the company's atmosphere and daily affairs. Companies are, however, free to offer more opportunities for you to learn about life at a software development company, such as attending the daily standup or other meetings, a tour, lunch, Friday-afternoon-drinks, etc.

        Generally, you'll be expected to be at the company between at least 9:00 and 16:00, though a company may have its own specific schedule.

        As part of the teamwork module, you're required to participate in a *Business safari* at least thrice. You are welcome to participate more often though - it can be a great way to find an awesome place to do an internship or to work!

        <div class="notification is-warning">
            Please note that this assignment counts as part of the module exam.
        </div>

    Planned editions: |
        | Octant | Date |
        | -- | -- |
        | 1 | Friday 2023‑10‑06 |
        | 5 | Friday 2024‑03‑22 |
        | 7 | Friday 2024‑06‑07 |

        On these days, our own offices will be closed. For students who are unable to attend on (a particular) Friday, and for companies who are unable to receive students on (a particular) Friday, we provide a backup option on the Thursday *before* the actual Safari date.

        In the week before a Safari, you'll be asked (through Discord) to register with a company of your choice. Spots per company are limited and on a first-register-first-serve basis. Be sure to notice the city and the date (Friday or backup-Thursday) for the company you're registering with. A couple of days before going on Safari, you'll be introduced to the company representative by email.

    Passing criteria:
    - |
        In order to pass this assignment, you need to...
        
        - Be present at the company between the expected times. When you leave, make sure to say bye and thanks.
        - Behave professionally and friendly. We do *not* expect to hear *any* complaints from the companies.
        - Spend 10 minutes to submit your reflection on the day afterwards.

    Reflection:
    -
        must: true
        text: |
            Upload a completed `reflection.md`.

