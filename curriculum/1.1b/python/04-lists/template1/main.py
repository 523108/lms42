# Define any functions you want to use here.


def main():
    print("*** Welcome to Tic Tac Toe ***")

    # Add you main menu code within this main() functions. This way,
    # you won't accidentally create global variables, because all 
    # variables introduced here will be local to the main function.


# Actually call the main function.
main()
