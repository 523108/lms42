======================================== Tokens ========================================
'"Hi mom!"' <string>
';' <delimiter>
'42' <number>
';' <delimiter>
'var' <keyword>
'x' <identifier>
';' <delimiter>
'var' <keyword>
'y' <identifier>
'=' <operator>
'3' <number>
';' <delimiter>
'var' <keyword>
'z' <identifier>
'=' <operator>
'"z\'s initial value"' <string>
';' <delimiter>
'' <eof>

======================================== AST ========================================
tree.Program(
    block=tree.Block(
        statements=[
            tree.Literal(value='Hi mom!'),
            tree.Literal(value=42),
            tree.Declaration(identifier='x', expression=None),
            tree.Declaration(identifier='y', expression=tree.Literal(value=3)),
            tree.Declaration(
                identifier='z',
                expression=tree.Literal(value="z's initial value")
            )
        ]
    )
)

======================================== Running program ========================================
