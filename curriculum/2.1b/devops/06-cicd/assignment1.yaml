- Introduction: |
    Use the CI/CD functionality from Gitlab to build applications and containers, run tests and deploy applications.

- Assignment:
    - Your first pipeline:
        -
            link: https://docs.gitlab.com/ee/ci/quick_start/
            title: Get started with GitLab CI/CD
            info: Explains how the GitLab CI/CD works.
        - 
            text: |
                For your first pipeline we will create a simple job that only prints some text int the Gitlab interface.

                Your task is to:
                1. Create a new GitLab repository within the Saxion organization using [this link](https://repo.hboictlab.nl/template/32020668).
                2. Clone the repository to your laptop.
                3. Create a `.gitlab-ci.yml` file (locally) in the root of your repository.
                4. Edit the `.gitlab-ci.yml` file (locally) so that it echo's 'Hello world!'.

                To validate whether you have successfully implemented this objective you should push your code to Gitlab and check (on https://gitlab.com) whether the CI/CD pipeline has run successfully. Also check whether you can see the 'Hello world!' message is printed in the build job stage.  
            must: true

    - Multiple build jobs:
        - 
            link: https://docs.gitlab.com/ee/ci/pipelines/pipeline_architectures.html#basic-pipelines
            title: Basic Pipelines
            info: This documentation explains how to create a basic CI pipeline.
        -
            text: |
                In the provided template you will find a sample python file and a CSV containing film actors.Add these files to your repository and validate that the application works correctly. Note that this application uses Poetry so you should initialize it as a Poetry application. The Poetry project files should also be added to your repository.

                After you have validated that the application runs successfully you should add a second build job to your `.gitlab-ci.yml` file. This build job should install the required Poetry packages (note that is should not initialize a Poetry project to do so). Validate that the packages have been installed in the Gitlab CI logs on https://gitlab.com. 

            ^merge: feature            

    - Automated unit testing:
        -
            text: |
                With continuous integration you can automate the testing for your application.

                Your task is to implement at lease 3 unit tests for the application. Examples of test could be:
                - Validate that all actors have been read successfully by doing a count on the number of lines read.
                - Validate whether the correct amount of actors have been filtered when doing a search on an actor name.
                
                Check whether they run successfully and if so, extend you CI to include a test job that runs after all the builds jobs. Also validate that the build fails when you (purposely) let a test fail.

            ^merge: feature            

    - Job artifacts:
        -
            link: https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html
            title: Job artifacts
            info: Jobs can output an archive of files and directories. This documentation explains how to achieve this in Gitlab CI.
        -
            text: |
                Sometimes a job can produce a result, this is called a job artifact. In our example we can run our application in such a way that it produces a chart in the form of a text file.

                Your task is to create a release job that stores the resulting chart as an artifact in Gitlab.
                Set the target path to `chart.txt` and the expiration period to one week. Validate whether the artifact is created correctly on in the Gitlab CI logs on https://gitlab.com.

            ^merge: feature            

    - Only product artifacts on merge:
        - 
            link: https://docs.gitlab.com/ee/ci/pipelines/merge_request_pipelines.html
            title: Pipelines for merge requests
            info: Documentation on how to create specific pipelines for merge requests.
        -
            text: |
                For our repository we would like to limit the amount of artifacts that are being created in order to save some disk space. We want to update our pipeline in such a way that our artifacts are only created when we merge a feature branch with the main branch. So on the feature branch the other jobs should run but only on the main branch should you build the release job that produces the artifact.

                Update your `.gitlab-ci.yml` file so that artifacts are created only when the feature branch is merged with the main branch.

                Validate that:
                1. A push to the feature branch does not produce a job artifact.
                2. A merge to the main branch does.

            ^merge: feature            
          
    # - Build and push containers:
    #     - 
    #         link:
    #         title:
    #         info: Iets over Gitlab variabelen.
    #     -
    #         text: |
    #             With the CI/CD tool you can also push containers to a container registry.
