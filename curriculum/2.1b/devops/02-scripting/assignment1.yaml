- Introduction:
    In this assignment we will start with the basic of Linux scripting. Here you will get the chance to build your own util toolbox.

- Assignment:
    - Higher or lower:
        -
            link: https://linuxconfig.org/generating-random-numbers-in-bash-with-examples
            title: Generating Random Numbers In Bash With Examples
            info: Shows how to generate random numbers in a shell script.
        -
            text: |
                As a primer into shell scripting we will ask you to build a simple game called **Higher Lower**.
                The rules of the game are:

                1. the program picks a target - a random number (between 0 and 1.000) - when it starts
                2. the program asks for the user to enter a number
                3. if the entered number is higher then the target the program prints "*Your guess was higher*"
                4. if the entered number is lower then the target the program prints "*Your guess was lower*"
                5. if the entered number is equal to the target the program prints that the guess is correct, displays the number of tries and exits. 
                6. as long as the target has not been guessed the program keeps asking the user to enter a number.

                - Implement this logic in the provided `higher_lower.sh` script. You can execute the script as follows:
                
                ```sh
                $ ./higher_lower.sh
                ```
            ^merge: feature

    - Write a safe delete:
        - 
            link: https://www.howtogeek.com/437958/how-to-use-the-chmod-command-on-linux/
            title: How to Use the chmod Command on Linux
            info: A detailed explanation of how to use the chmod command.
        -
            link: https://www.tutorialspoint.com/unix/unix-special-variables.htm
            title: "Special variables in linux"
            info: Special variables which could be useful for your script.
        -
            text: |
                Write a command line script that a can "safe" delete a file, which can be restored later on (much like a trash can). 
                - create a file called `trash.sh` 
                - add the proper *shebang* at the beginning of the script
                - set the file mode so that it can be executed (using the **chmod** command).
                 
                Executing the script without any arguments should display the following help menu.
                ```sh
                Usage:
                 trash <file>     : Moves the file to the trash.
                 trash -l         : Lists all the files in the trash.
                 trash -r <file>  : Moves the file from the trash to the current directory.
                ```

                Filenames passed as command-line arguments to this script are not deleted, but instead moved to a **/home/username/trash** directory. Be sure to create the directory if it does not yet exist. 
                
                An example of how the script would work looks like this:
                ```sh
                $ touch delete.me
                $ ls
                delete.me
                $ ls ~/trash
                $ ./trash delete.me
                $ ls
                $ ls ~/trash
                delete.me
                $ ./trash -l
                Currently in trash:
                total 0
                -rw-r--r--  1 tse01  staff  0 Jun  7 13:35 delete.me
                $ ./trash -r delete.mine
                ERROR: No file or directory with the name 'delete.mine' in trash.
                $ ./trash -r delete.me
                Restored 'delete.me'.
                $ ls
                delete.me
                $ ./trash -l
                Currently in trash:
                $ _
                ```
            ^merge: feature

    - Write an archiver:
        -
            link: https://linux.die.net/man/1/inotifywait
            title: inotifywait(1) - Linux man page
            info: The man page
        -
            link: https://linux.die.net/man/1/date
            title: date(1) - Linux man page
            info: The man page
        -
            link: https://linuxhandbook.com/symbolic-link-linux/
            title: What is Symbolic link in Linux and why is it used?
            info: A symbolic link, also known as a symlink or a soft link, is a special type of file that simply points to another file or directory.
        -
            text: |
                Your task is to create an archive utility script (call it `archiver.sh`). The purpose of the script is to automatically archive files in a certain directory structure. In the archive files are organized in the following directory structure: `~/archive/<year>/<month>/<day>` (note that the "`~`" stands for your home directory `/home/username`).
                
                The way the archive script works is as follows:
                
                1. In the home directory there should be a folder called `archive` (which is created if it does not exist).
                2. The script listens for files that have been moved to the `archive` directory (so only moved files not files that have been copied to this directory). The **inotifywait** command (see man page) can be used to listen for *move_to* events in the archive directory (see man page). You will need to install the '*inotify-tools*' package from add/remove software in order to be able to use it.
                3. If a file is moved to this directory, the script will be moved it from the `archive` directory to the correct (sub) directory, for example `archive/2022/05/24/`. If the (sub) directory does not exist it should be created. *Hint*: You can use the Linux **date** command to get the current year, month and day (see man page).
                4. The **inotifywait** will not tell you the name of the file that has been moved to the `archive` directory. Instead your script will need to list the files in this directory that have not yet been added to a sub directory (an `archive/<year>/<month>/<day>` folder that is). *Hint*: Use the **find** command to list only files in the `archive` directory (excluding directories) and not in the sub directories (something with *maxdepth*).
                5. Lastly your program should create a symbolic link on the Desktop that links to the archive folder. This makes it easy to move the files from your desktop to your archive.
            ^merge: feature

    - Extend the archiver:
        - 
            link: https://linuxize.com/post/how-to-run-linux-commands-in-background/
            title:  How to Run Linux Commands in Background
            info: A background process is a process/command that is started from a terminal and runs in the background, without interaction from the user. This article explains how this works on Linux.
        -
            text: |
                Extend your archiver script so that it can run in the background. Update your script so that:
                1. it takes **start** as a command line argument which runs the script in the background (using the ampersand '**&**' after the command).
                2. it takes **stop** as a command line argument which stops the script. Stopping the script can be done using the `kill <PID>` command. If you want to find the PID of a running process you can run `pgrep archiver.sh`. 
                
                With this information is should be possible to adjust your script so that you can run `./archiver.sh start` which creates the necessary folders and links and starts listening for files (in the background), and `./archive.sh stop` which locates the PID of the archive script, kills it (and maybe remove the symbolic link from the desktop, but keeping the archive folder in the home directory).  
            ^merge: feature
            weight: 0.5
            
