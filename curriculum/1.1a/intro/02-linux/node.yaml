name: Linux
description: Install Manjaro Linux on your laptop and get comfortable with it, as that's the operating system we'll be using for the rest of the programme.
public: true
goals:
    linux: 1
assignment:
    Manjaro Linux?:
        - |
            During all of this study programme, we will assume you're working with the KDE edition of the Manjaro distribution of Linux, which we'll be installing today.
        
        -
            link: https://www.youtube.com/watch?v=E0Q9KnYSVLc
            title: "The Making of Linux: The World's First Open-Source Operating System"
            info: A nice introductory video on Linux. What's a kernel? What's a distribution? What are desktop environments? What is package management?
            
    Preparations: |
        In order to install Manjaro you need a compatible computer. Fortunately, nowadays Linux runs on most systems without any (major) problems. Apple MacBooks are an exception: important features such as Wi-Fi, audio and suspend don't always work, depending on the exact hardware version. There's an easy way to find out though: most Linux distributions can be booted from a USB-stick, without modifying anything on the internal drive. That way, you can see if everything works before installing.
        
        There are three options to choose from:
            
        1. Install Manjaro *replacing* your existing operating system and erasing any files.
            - You need to be really sure that there are no important files on the computer before you start the installation process.
        2. Install Manjaro *besides* your existing operating system (Windows or OS X). This means that every time you turn on your computer, you get to choose which Operating System to start. This is called 'dual boot'.
            - You need to make sure there's at least, say, 40 GB (Gigabyte) free on your drive. That should be enough space to install Manjaro and store all files required for this study programme over the next two years. That's assuming you won't be using Linux for any serious gaming, creating a large video collection, or something like that. More is better, of course.
            - You need to backup any important files. Most likely everything will be just fine, and your files will be right where you left them. But just in case something weird happens, you wouldn't want to lose your carefully collected 1990's game collection, would you? For backup you can use any online storage you may have, or an empty USB stick. There should be a few available in the classroom.
        3. Install Manjaro in a *virtual machine*. This can be slow and can cause all kinds of weird problems. We would generally **not** recommend this, unless you're stuck with a laptop that is not compatible with Linux. In case you have a MacBook, you can probably complete most assignments using OS X and use the Linux virtual machine only when you run into problems. Instructions will however be written only with Manjaro in mind, and teachers may not be able to help you.
            - Your laptop needs to have enough RAM memory to run two Operating Systems at the same time. With less than 16 GB things would probably get *really* slow.
            - You need to make sure there's at least, say, 25 GB (Gigabyte) free on your drive. That should be enough space to install Manjaro.
            - You need to install VirtualBox from: https://www.virtualbox.org/

    Create an installation medium:
        - "The easiest way to install Manjaro is from a USB stick that has the installation files on it. If such a stick is available in the classroom: great, use it, and skip to the next step!"
        - Otherwise you'll need to download Manjaro version 20.10. There are two downloads you can choose from. In case you have an NVIDIA graphics adapter, make sure you download the one containing the appropriate drivers.
        -
            link: https://manjaro.org/download/
            title: Manjaro - Download
            info: Download the full X86_64 Image of the *Plasma Desktop* edition of Manjaro here. The resulting `.iso` file should be a couple of gigabytes in size.
        - "Next, you'll need to obtain a USB stick that may be completely erased. You can't just copy the file you just downloaded onto it, but you need image flashing software for that. We recommend:"
        -
            link: https://www.balena.io/etcher/
            title: Etcher - flash OS Images to SD cards & USB drivers
        - Download, install and run Etcher. Select the file you just downloaded and the USB drive you want to overwrite, and it will create a *bootable* USB stick for you.

    BIOS settings: |
        Before attempting to install Manjaro, it's a good idea to check if your laptop is configured appropriately in the BIOS settings. To open the BIOS settings, you can usually press some key (obsessively) just after turning on your computer. This might help:

        - Acer: F2 or DEL
        - ASUS: F2 for all PCs, F2 or DEL for motherboards
        - Dell: F2 or F12
        - HP: ESC or F10
        - Lenovo: F2 or Fn + F2 or F1 or Enter + F1
        - Samsung: F2
        - Sony: F1, F2, or F3
        - Toshiba: F2

        Within the settings interface, there are three things to look for:

        - Most laptops have a *Secure boot* option, which should be **disabled**. If you're seeing this setting but are unable to change it, you may need to set a *supervisor password* first.
        - Your Windows may be using *system encryption*. If this is enabled in the BIOS, first boot back into Windows to disable it there (search for *BitLocker* in the *start* menu), before disabling it in your BIOS settings. If you don't disable it in Windows first, you will no longer be able to start Windows.
        - Make sure booting from a USB drive is enabled. Alternatively, sometimes a *F12 boot menu* (or similar) can be enabled, allowing you to choose from which drive to start the operating system by (repeatedly) pressing F12 while booting.
        - If your laptop supports *RAID* or *RST*, make sure it's **disabled** and AHCI is enabled instead. This setting could be under something like *Storage* or *SATA*. Some laptops (I'm looking at you Acer) may have this setting hidden by default, until you press some secret key combination (possibly `ctrl-s`). If you can't find any setting like this, just continue with the installation process. If later on you run into the problem that Manjaro can't find your SSD drive, you should come back to searching for this setting.
        - Some laptops have the *Intel Optane* feature. You can leave this on initially, but in case the Manjaro installer (in the next step) crashes, you should turn this off.

    Installing Manjaro:
        - Insert the bootable Manjaro USB stick into your computer. Next turn on (or reboot) your computer, and have it boot from the USB stick.
        -
            link: https://www.howtogeek.com/129815/beginner-geek-how-to-change-the-boot-order-in-your-computers-bios/
            title: How to Boot Your Computer From a Disc or USB Drive

        - If all goes well, a full Manjaro will now be running! It hasn't touched your hard drive (yet), it's running directly from the USB stick. This is a good time to see if Manjaro runs well on your system. Play around a bit. See if it's fast enough (starting applications will be a faster once you've installed the system), the track pad works well, the keyboard is configured correctly, sound works, etc. Note that any settings you change here or applications you install will not be preserved - for that we need to install first. If there seem to be any problems/weirdness, ask for help!
        - Happy with how things work? Click the *Install Manjaro Linux* icon on the desktop to start the actual installation.
        - Answer the (easy) questions asked by the installer.

            - Stick with *American English* as the language.
            - At some point, the installer asks where on your hard drive Manjaro should go. Choose 'Install alongside' if you want to keep your current operating system. Choose 'Erase disk' if you don't (or if you're installing Linux inside VirtualBox). Choose "Swap (with hibernate)". If you're unsure about anything here, ask for help!
        - After a reboot, your new Operating System will be started. Hurray!

    Connect to the network: |
        In the system tray (bottom right corner of the screen) there should be a WiFi icon. Click it to view the list of wireless networks. Select `eduroam`. Use something like the following settings in the security tab:

        - Anonymous identity: 12345**@student.saxion.nl**
        - Domain: **saxion.nl**
        - User: 13245**@student.saxion.nl**
        - Password: himom!

        You should be able to connect to the internet now.
            
    Play around with the UI: |
        Take some time to explore your new Operating System.
        - What applications are installed? What can they do?
        - Where are the settings? Is everything to your liking?
        - Can you find a quick way to start applications?

    Install essential software: |
        Open the *Add/Remove Software* application. Click the menu icon in the top right, and open *Preferences*. Enable *AUR* support, the *Arch User Repository*. It allows you to install many more applications than those provided by Manjaro by default. You'll also get automatic upgrades. Take care though: applications offered from *AUR* are not officially supported by Manjaro, so install AUR packages only if you really need to! 
        
        While you're in preferences, select *The Netherlands* as the mirror location and click the *Refresh mirrors* button, to make sure that software won't be downloaded from halfway across the planet.

        Next, install the following applications:
        - Code - OSS &rarr; The Open Source version of Visual Studio Code, the code editor we'll be using through the study programme.
        - Discord &rarr; The chat/voice/video service we'll use for digital communication.
        - And of course anything else you like - it's free! Spotify? Steam?

    Upgrade software: |
        Manjaro Linux is always updating its many software packages. Even when installing from a freshly created USB stick, there will already been some packages that want updating.

        Go to the *Updates* tab of the *Add/Remote Software* application and update all software. (If it says everything is up to date, make sure that this is really the case by clicking the refresh link.)

        You'll want to keep updating your system about once every week. A red up-arrow icon in your system tray will remind you of this when there are updates available.

    Browser: |
        By default Manjaro will come with Firefox installed, which is a pretty good browser with a decent focus on privacy. If you prefer, you could install Chromium using *Add/Remove Software*, which is the Open Source version of Google Chrome. Fun fact: Chromium is Open Source because it is based on Apple Safari, which is Open Source because it is based on Konqueror, which is the (now defunct) browser that was created for KDE, the desktop interface you're currently using.

        *Pro tip:* As we'll be using many instruction videos on YouTube, it's probably a good idea to add the 'Enhancer for YouTube' extension to your browser. After installing it, go to the extension's settings and enable the *Hide related videos* check mark, to prevent YouTube from tempting you with your favorite 'related' distractions while you're trying to watch an instruction video. Furthermore this extension will skip ads and allows you to speed up and slow down videos using ctrl-scroll-up/down. 

    # Set up VPN: |
    #     For extra security, some Saxion web applications (such as *Bison*, for viewing your official grades) can only be accessed from within the Saxion network. If you want to use them from home, you will need a secure VPN connection to the Saxion network. To make such a connection, you'll need to install *python-eduvpn-client* using *Add/remove software*.

    #     If all goes well, the *eduVPN* application should show up in your application menu. Start it and follow its instructions to set up a VPN connection. After setting everything up, you can close the connection again, as you don't actually need it while at Saxion. Do this by clicking the WiFi-icon in the system tray and clicking *Disconnect* for the *eduVPN* connection.

    Join our Discord: |
        After finishing installation, start Discord, create an account or login, and join the [SD42 Server](https://discord.gg/UgnKCA9Sda). Test that audio/video work as they should.

    Linux installation and usage:
        text: |
            For this objective, you don't need to submit any work. During your feedback session with a teacher, you'll just show her/him around your Manjaro.
        1: There are still some problems with the installation, and/or the student cannot operate the system properly.
        3: Pretty workable installation, and the student is still adjusting but will probably be fine.
        4: Everything installed as detailed, and the student seems to be getting along with the GUI just fine.

    Linux basics:
        - To just be a regular Linux *user*, there's not much more to know. But programmers are *power users*. So let's explore what's underneath the shiny user interface!
        -
            title: Linux File System/Structure Explained!
            link: https://www.youtube.com/watch?v=HbgzrKJvDRw
            info: On Linux, files and directories are organized a bit differently than you may be used to on Windows or on OS X. This video explains.
        -
            title: Command-line for beginners
            link: https://ubuntu.com/tutorials/command-line-for-beginners#1-overview
            info: Programmers often use the command-line. This tutorial explains what it is, why we use it and how the basics work. This is important stuff to know! On Manjaro KDE you can open a command-line with the *Konsole* application. In section 7, this tutorial is using the `apt` command to install new software. This is Ubuntu-specific and doesn't work on Manjaro. Feel free to just skip that part.

    Install the lms application: |
        You'll need to download and install a small command-line application to download and upload (completed) assignments from/to this website. Open up a terminal and execute this command:

        ```sh
        wget https://sd42.nl/static/lms -q -O - | python3 - install
        ```

        This should have installed the `lms` application. Try if this is indeed the case by running:

        ```sh
        lms login
        ```

        If `lms` was installed properly, it should give you instructions on how to connect it to your user account on this website. Follow the instructions. 

    Use lms to work on the assignment: |
        Have the `lms` create a directory for you to work on the current assignment, and have it put any template files (provided by the teachers) in this directory. To do that, simply type:
        ```sh
        lms template
        ```

        It should show you where the directory was created. This is where you can do your work for the day. As there will be *many* assignments, the `lms` application tries to sensibly organize them automatically. You are strongly advised to stick to this organization, and not move files/directories around.

        A shortcut to get to work quickly is:
        ```sh
        lms open
        ```
        It will download the template if you don't have it yet, and open the assignment directory in Visual Studio Code.

        After you've completed working on the assignment (by editing/creating files in the provided assignment directory), you need to upload your work to this website (usually before the end of the working day). To do this execute:
        ```sh
        lms upload
        ```

        This only uploads the work, but doesn't submit it for review. To do that, click the 'Submit work' button on the bottom of this web page (but only *after* you have uploaded your work).

        With that said, you should now know enough to work on your first assignment objective!

    Using Visual Studio Code:
    -
        link: https://www.youtube.com/watch?v=B-s71n0dHUk
        title: Learn Visual Studio Code in 7min (Official Beginner Tutorial)
        info: |
            A quick introduction to Visual Studio Code, the code editor that we'll be using for all modules in the first two periods. Feel free to ignore any programming mumbo jumbo you don't understand. It'll all make sense in a couple of weeks! :-)

    Command-line:
        text: |
            Demonstrate your command-line super powers!

            1. Within the assignment directory (`~/lms/1.1a/intro/linux`), create a directory in a directory in a directory. Put the command(s) you used in the `command-line.md` file in your assignment directory, replacing the `TODO` placeholder.
            2. Count the number of files and directories that are in the `/usr/bin` directory and its subdirectories, by combining the `find` and the `wc` commands using a *pipe*. You can use `man` (or Google) to find information about these commands. Put your solution in `command-line.md`.
            3. Like before, but this time only file names containing the letter `x` should be counted. Use the `grep` command to filter the output of `find`.
            4. `/proc/cpuinfo` is a 'fake' file (it doesn't exist on disk, but is generated by the Linux kernel when it is read) that contains a lot of information about the CPU cores in your computer. Sort the contents of this file line-by-line alphabetically and redirect the output of the sort to a file that you'll call `cpu-sort.txt` in the assignment directory. Copy the command you used into `command-line.md`.
        0: Really needs more practice!
        2: 1/3 perfect, at least 1 other is pretty close.
        3: 2/3 perfect.
        4: Perfect.

    Upload your photo:
        text: |
            Please upload a photo of your (actual! close-up! full! recognizable!) face on your LMS profile page. This should help your fellow students and teachers to learn your name.
        must: true

    Forward your Saxion email: |
        Many students tend not to check their Saxion email very often. Especially since it requires annoying two-factor authentication. However, you may be receiving important email there. Fortunately, there's a solution, assuming you have another email address that you *do* in fact check regularly. Configure your Saxion account to [forward all email](https://outlook.office.com/mail/options/mail/forwarding) to your other address. 


    Submit!: |
        Don't forget to `lms upload` and submit your work!
