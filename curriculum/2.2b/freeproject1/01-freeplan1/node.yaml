name: Plan your project
description: Come up with a great idea and make sure it's doable as well as usable within your portfolio.
allow_longer: true
public: users
grading: false
upload: true
resources:
-
    link: https://blog.producthunt.com/how-to-come-up-with-side-project-ideas-4a2c8049deba
    title: How to come up with side project ideas
-
    link: https://www.indeed.com/career-advice/resumes-cover-letters/programming-side-projects-to-boost-your-resume
    title: 14 Programming Side Projects To Boost Your Resume

assignment: 
- |
    Within a *free project* you can work on a project of your own choosing for a period of four weeks. As the project is not graded, you are free to do just about anything you like. You can start something new, or contribute a (largish) feature to an existing (Open Source) project, or continue work on an earlier project of yours. You can focus on implement, on technical/function design, or both. Anything goes. However:

    - In your [graduation portfolio](/curriculum/graduation) you will need to demonstrate your mastery of the end qualifications. You will probably want to strategically create a project assignment with the graduation assignment in mind. Your graduation supervisor can help you with that.
    - For many software development companies, a well-stocked GitHub profile is worth more than an impressive resume. Therefore, it may be worth considering creating an Open Source project.
    - The project provides an opportunity to explore new technical directions that seem interesting. It can be a great way to find out what type of software you would (not) like to work on after you graduate.
    - Try to pick something that's fun and/or useful to you! That makes it a lot easier to build something impressive.

    Here are the steps you should follow:

    1. Come up with an idea.
    2. Elaborate on your idea in a Markdown file, as explained in the objective below.
    3. Submit this assignment, and await feedback from your graduation supervisor, whom will try to assess if the scope and difficulty level of your plan are appropriate, and if your graduation portfolio goals are reasonable.
    4. When your plan is approved, you can start the actual work! While working, you should maintain a daily journal.
    5. After about 20 working days (you're allowed to put in more time, but preferably not of course) you should submit your finished work and update your graduation portfolio.
    6. You finish the project by doing a *show and tell* about it for your class. This involves a quick demo, and telling something about the technical choices that you made, after which the class (and teacher) can question you about these choices.
-
    text: |
        In a Markdown file, write a brief initial project plan that includes at least:

        - A high-level description of what you're going to build.
        - Which techniques will you be using. Which of these will you need to learn first?
        - A work break-down into at least 4 activities, each with a description and a time estimate.
        - Which graduation portfolio qualification parts do you intend to address in this project? This should also be reflected in your `README.md`.
        - Which 20 days you are planning to work on the project, in case you will not be doing the project full-time.
        - Which parts of your project, if any, may take a lot longer than you think now, or may turn out to be impossible/impractical? How would you deal with that? Are you okay with taking on this risk?
        - What's your strategy for dealing with delays? In case things take significantly longer than predicted, what will you do? Keep working at it after the four weeks are over? Or is there some part of the work that can be dropped/simplified, while still addressing the portfolio goals?

        Your plan doesn't need to be perfect. It's okay to adapt as you go. This initial version of the plan *should* however give some confidence that what you'll be doing is worthwhile.

        After submitting your plan, please ask your graduation supervisor to approve it. (Another teacher may be able to approve it as well, but your graduation supervisor probably has a better view of the big picture.)
    must: true
