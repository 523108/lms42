describe("Example", function() {
	it('is successful', function() {
		cy.request({
			url: '/',
		}).then(response => {
			expect(response).property('body').property('success').to.equal(true);
		});
	});

    it('has no example page', function() {
		cy.request({
			url: '/example',
			failOnStatusCode: false
		}).then(response => {
			expect(response).property('status').to.equal(404);
			expect(response).property('body').property('error').to.equal("Invalid resource");
		});
	});
});
