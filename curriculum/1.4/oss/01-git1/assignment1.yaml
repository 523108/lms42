- |
    Introduction to Git. In this assignment you will learn the basic Git commands. For now you will learn how to use Git from the command line. There are some fancy tools for managing your Git repository. Unfortunately you are not allowed to use them in this assignment.

- Tasks:
  - Install Git: |
      Go to this link containing details on how to install Git in multiple operating systems: https://git-scm.com/book/en/v2/Getting-Started-Installing-Git
        
  - Configure Git: |
      Before you really get started with Git, it is important that you have set up the configuration of Git (on your laptop) correctly. Go to your terminal and type the following (use your Saxion student email address):
      ```
      git config --global user.name "<your name>"
      git config --global user.email <your email address>
      ```
      You can check whether the configuration has been set up correctly with the following commands:
      ```
      git config --global user.name
      git config --global user.email
      ```
      Note: These settings are specific to your laptop. If you use another machine in addition to your laptop to adjust your code, you must also adjust the Git configuration on this machine as indicated above.

  - Command-line file editing: |
      When using command-line applications like `git`, they will typically start up the *Vim* command-line editor when they want you to input/edit some text. Vim is a powerful, efficient, and customizable text editor that can be a bit challenging for beginners.

      Here are some basics of Vim that you might find useful:

      - Modes: Vim has two modes, the command mode and insert mode. When you first open Vim, you will be in the command mode. To start typing, you need to switch to insert mode by pressing the 'i' key.

      - Navigation: Vim has many keyboard shortcuts for navigation. For instance, you can use the arrow keys or the 'hjkl' keys to move the cursor left, down, up, or right, respectively. You can also use the 'w' and 'b' keys to move the cursor word-by-word.

      - Editing: In Vim, you can delete, copy, paste, and replace text using various commands. For example, to delete a line, you can press the 'dd' keys, and to paste a line, you can press the 'p' key.

      - Saving and quitting: To save changes to a file, you can press the ':w' command in command mode, and to quit Vim, you can press ':q'. If you have made changes to a file and want to save them and quit at the same time, you can type ':wq'.

      If you are a beginner and find Vim challenging, you can set up an easier default editor. Here's how to set up [Micro](https://micro-editor.github.io/) as your default editor when you're using `zsh` (the Manjaro default shell):

      - Install `micro` using *Add/Remove Software*.
      - To configure Micro as your default editor just for one terminal session run:
        ```sh
        export EDITOR=micro
        ```
      - To configure it as the default for all future (but not any running) terminal sessions, add the above line to your `zsh` configuration file using this command:
        ```
        echo 'export EDITOR=micro' >> ~/.zshrc
        ```
      
      Micro should be pretty straightforward to use. Relative to Vim at least. :-)



- Assignment:
    
  - Create a git repository:
    -
      text: |
        In the template folder you will find some files we want to add to a Git repository. Create a new folder - called **git** - in the template folder. Initialize a Git repository (using git init) in this folder and add the 'README.md' and 'diffofill.sh' files from the template folder to the git repository. Note that adding the files is not (only) a matter of copy and pasting the files in the proper directory. You should use the 'git add' and 'git commit' commands to add the files to the repository. As a commit message you could use 'Initial commit'.

        You can use 'git log' to verify your results, which should look something like this:
        ```
        $ git log
        commit ecea92461623bfb73e98802632cee4e54cc0caa8 (HEAD -> master)
        Author: Timothy Sealy <t.e.sealy@saxion.nl>
        Date:   Wed Apr 7 10:21:21 2021 +0200

            1: Initial commit (objective 1)
        ```

        Tip: For this assignment it is a good idea to add the objective numbers to your commit messages. This way you can relate the commits to specific objectives.

      ^merge: feature
      code: 0
      weight: 0.5

  - Implement a diff script:
    - 
      link: https://www.atlassian.com/git/tutorials/saving-changes
      title: Saving changes (git add)
      info: The git add command adds a change in the working directory to the staging area. It tells Git that you want to include updates to a particular file in the next commit.
    - 
      link: https://www.atlassian.com/git/tutorials/saving-changes/git-commit
      title: Git commit
      info: The git commit command captures a snapshot of the project's current changes. Committed snapshots can be thought of as “safe” versions of a project — Git will never change them unless you explicitly ask it to!
    -
      text: |
        Your git repository now contains a shell script called **diffodill.sh**. In this file implement a script that computes the difference between two files using the `diff` command. The files are passed as arguments to the script. The script should print the usage when no arguments are given and an error when an  invalid number of arguments are passed. You can use the files in the example directory for testing. 

        When you are finished be sure to commit your changes to your git repository. Please write meaningful commit messages (for example "2: Implemented diff").
      ^merge: feature
      code: 0

  - Undoing mistakes:
    -
      link: https://www.atlassian.com/git/tutorials/undoing-changes/git-revert
      title: Git revert
      info: The git revert command can be considered an 'undo' type command, however, it is not a traditional undo operation.
    - 
      text: |
        Extend your script so that it shows the differences in colors (use the --color flag). Commit this change (give it number "3a"). 

        Come to think of it we do not really want to have our results colorized. Undo the change. Make sure that the head of the branch is the same as before the color change (use "git diff <commit_hash_1> <commit_hash_2>" to check this).

        Tip: Use the '--no-edit' flag to skip editing the commit message. 
      ^merge: feature
      code: 0
      weight: 0.5

  - Implement a new feature:
    -
      link: https://www.youtube.com/watch?v=S2TUommS3O0
      title: Creating and merging branches in Git
      info: Learn the basics of creating and merging branches in Git. In Git, branches are a part of your everyday development process. Git branches are effectively a pointer to a snapshot of your changes.
    -
      link: https://www.freecodecamp.org/news/gitignore-what-is-it-and-how-to-add-to-repo/
      title: "Gitignore Explained: What is Gitignore and How to Add it to Your Repo"
      info: The title says it all ;)
    -
      text: |
        It is now time to implement a new feature in the script. Be sure to implement the changes on a new branch. First create this branch and switch to it before implementing the following changes.

        Extend the script so that the result of the diff is stored in to a file. The file (result.diff) should be stored in a directory called 'output'. Make sure that the file (results.diff) is not tracked by git, because this is generated when the script is run and depends on the arguments you pass to the script (hint: use gitignore). 
        
        Also note that git does not track empty directories. Your script depends on the 'output' directory to store the results.diff file so you should add a '.keep' file in this directory and check it into git.

        Use branching to implement your new feature. Be sure to merge the branch with master.
      ^merge: feature
      code: 0

  - Implement more features:
    -
      link: https://www.youtube.com/watch?v=xNVM5UxlFSA
      title: How to resolve merge conflicts in Git
      info: A video explaining how to resolve a merge conflict.
    -
      text: |
        You can pass the '--side-by-side' argument to the diff command to get a side by side comparison (thank you Captain Obvious :). Your assignment is to implement this feature in the script on a NEW branch. Commit your change (number 5a) but do NOT merge the branch yet.

        After you have implemented this feature on this branch switch back to the master branch. On the master branch change your script so that the name of the file containing the results is defined in a separate variable. Create a variable (OUTPUT_FILE) and set its value to './output/results.diff'. Update the diff command in the script so that it uses this variable. Commit this change (5b) on the master branch.

        Now merge the branch containing the side by side comparison into your master branch. Resolve the merge conflict (if you have none please contact your mentor ;).
      ^merge: feature
      code: 0

  - Revert a merge:
    - 
      link: https://www.datree.io/resources/git-undo-merge
      title: Git undo merge
      info: Reverting a merge is a little trickier than a reverting a normal commit. In the post the details of undoing a merge is explained.
    -
      text: |
        Unfortunately we are not satisfied with the result of our merge. Your job is to revert the master branch to the point it was before the merge.
      ^merge: feature
      code: 0
      weight: 0.5

  - Let's rebase:
    -
      link: https://www.youtube.com/watch?v=kMvLn8WcAII
      title: Git rebase tutorial. Rebase vs Merge
      info: Learn how to use Git Rebase and how is it different from Git Merge.
    -
      link: https://www.atlassian.com/git/tutorials/rewriting-history/git-rebase
      title: Bitbucket - git rebase
      info: An in-depth discussion of the `git rebase` command.

    - 
      text: |
        Let's try out rebasing.
        - First create a feature branch called `my_feature_branch`.
        - Switch back to the `master` branch.
        - Create a new file as follows:
          ```sh
          $ git log --oneline > master.gitlog
          ```
        - Commit (7a) this file to the `master` branch.
        - Switch back to the previously created feature branch.
        - Do the same on this branch (and save the git log result in `branch.gitlog``).
        - Commit (7b) this file to the feature branch.

        At this point your feature branch is running behind on the master branch (*and* the other way around). In order to include the latest changes in the feature branch we will rebase the master branch on the feature branch. Let's analyze what happens during a rebase:
        1. Create a file in the root of the git directory called 'rebase-analysis.txt'. Execute a 'git log --oneline' and paste the output in the file and save it.
        2. Rebase the master branch on the feature branch.
        3. Execute a 'git log --oneline' again and paste the output in the file and save it.
        4. Look at the two git log outputs and write down your explanation of what has happened. Hint look at the commit hashes.

        When you analysis is complete, commit (7c) the file to the feature branch and merge the branch with master.
      ^merge: feature
      code: 0

  - Solving rebase conflicts:
    - 
      text: |
        - Create a new feature branch but stay on master.
        - On the master branch implement the side by side comparison in your script and commit it (7a).
        - Switch to the previously created feature branch and implement colorized diff and commit it (7b).

        Let's rebase master on the feature branch. The rebase should give a conflict (if you have none please contact your mentor ;). Carefully read the git error message for tips on how to solve the rebase conflict.

        After a successful rebase the git log of the feature branch should look something like this:
        ```
        7077a01 (HEAD -> feature_4) 7b: Implemented colorized diff (again)
        f617eda (master) 7a: Added side by side comparison (again)
        075e718 (feature_3) 6c: Added rebase analysis
        9d6e2c9 6b: Added gitlog of feature branch
        ae1b243 6a: Added gitlog of master
        21c3d7d Revert "5c: Merge branch 'feature_2'"
        0ed9bea 5c: Merge branch 'feature_2'
        04650eb 5b: Added variable for the result filename
        7e59d69 (feature_2) 5a: Implemented side by side comparison
        96ba96e (feature_1) 4b: Added .gitignore
        750482f 4a: Extended script to write results to file
        039dc74 Revert "3a: Implemented colorized diff"
        fc79fe1 3a: Implemented colorized diff
        10a9d67 2: Implemented diff
        64c6631 1: Initial commit
        ```
      ^merge: feature
      code: 0
      weight: 0.5
