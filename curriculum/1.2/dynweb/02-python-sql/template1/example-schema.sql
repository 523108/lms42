-- To (re)create a database with this schema, use the following shell command:
-- poetry run postgresqlite < example-schema.sql 

DROP SCHEMA public CASCADE;
CREATE SCHEMA public;

CREATE TABLE test(
    id SERIAL PRIMARY KEY,
    gibberish TEXT NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL 
);
