User tasks:
1. (example: As a student i want to complete write down a task for others to perform in this user study)

2.

3.


List of test users:
1.
2.


Findings (per task):
Task 1:
- User 1: 
    - (Write you finding for this tasks here)
- User 2: 
    - 

Task 2:
- User 1: 
    - 
- User 2: 
    - 

Task 3:
- User 1: 
    - 
- User 2: 
    - 


Write down the strengths (pros) and weaknesses (cons) of both designs. 
Base your analysis on the feedback you have received from the test users.

|             | Pro                  | Cons                 |
| ----------- | -------------------- | -------------------- |
| Primary     | - (write a pro here) | - (write a con here) |
|             | - ...                | - ...                |
|             |                      |                      |
| Alternative | - ...                |                      |
|             | - ...                |                      |
